package api

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/spf13/viper"

	"encoding/base64"

	"encoding/json"
	"strings"
)

var (
	cache              = make(map[string]interface{})
	ErrVersionNotFound = errors.New("Version Not Found")
)

type Client struct {
}

func callGet(url string) []byte {
	return _callWithoutData(http.MethodGet, url)
}

func _callWithoutData(method string, url string) []byte {
	if viper.GetBool("debug") {
		fmt.Println("_callWithoutData", method, url)
	}
	req, _ := http.NewRequest(method, url, nil)

	authBasic := base64.StdEncoding.EncodeToString([]byte(viper.GetString("jira.username") + ":" + viper.GetString("jira.password")))

	req.Header.Add("authorization", "Basic "+authBasic)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
	}

	return body
}

func callPut(url string, data []byte) []byte {
	return _callWithData(http.MethodPut, url, data)
}

func callPost(url string, data []byte) []byte {
	return _callWithData(http.MethodPost, url, data)
}

func _callWithData(method string, url string, data []byte) []byte {
	if viper.GetBool("debug") {
		fmt.Println("_callWithData", method, url, string(data))
	}
	req, _ := http.NewRequest(method, url, bytes.NewBuffer(data))

	authBasic := base64.StdEncoding.EncodeToString([]byte(viper.GetString("jira.username") + ":" + viper.GetString("jira.password")))

	req.Header.Add("authorization", "Basic "+authBasic)
	req.Header.Add("Content-Type", "application/json")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Print(err)
	}

	return body
}

func GetAllVersion(projectName string) []Version {
	if _, ok := cache[projectName]; !ok {
		url := viper.GetString("jira.url") + "/rest/api/2/project/" + projectName + "/versions"
		body := callGet(url)

		var response []Version
		json.Unmarshal(body, &response)

		cache[projectName] = response
	}

	return cache[projectName].([]Version)
}

func GetLastVersion(projectName string) string {
	url := viper.GetString("jira.url") + "/rest/api/2/project/" + projectName + "/version?startAt=0&maxResults=1&orderBy=sequence"
	body := callGet(url)

	var response VersionPaginated
	json.Unmarshal(body, &response)

	url = viper.GetString("jira.url") + "/rest/api/2/project/" + projectName + "/version?startAt=" + strconv.Itoa(response.Total-1) + "&maxResults=1&orderBy=sequence"
	body = callGet(url)
	json.Unmarshal(body, &response)

	versions := []string{}
	for _, v := range response.Values {
		versions = append(versions, v.Name)
	}

	return versions[0]
}

func CreateNewVersion(projectName string, version string) string {
	url := viper.GetString("jira.url") + "/rest/api/2/version"

	v := Version{
		Name:    version,
		Project: projectName,
	}
	jsonByte, _ := json.Marshal(v)

	body := callPost(url, jsonByte)

	return string(body)
}

func UpdateIssue(id string, update []byte) string {
	url := viper.GetString("jira.url") + "/rest/api/2/issue/" + id

	body := callPut(url, update)

	return string(body)
	/*
			['update' => ['fixVersions' => [
		            ['set' => [['name' => $version]]]
		        ]]]);
	*/
}

func VersionExists(projectKey string, version string) bool {
	found := false
	for _, v := range GetAllVersion(projectKey) {
		if v.Name == version {
			found = true
			break
		}
	}

	if !found {
		return false
	}

	return true
}

func UpdateIssueVersion(id string, version string) (string, error) {
	splitted := strings.Split(id, "-")
	projectKey := splitted[0]

	if !VersionExists(projectKey, version) {
		return "", ErrVersionNotFound
	}

	update := fmt.Sprintf(`{"update": {
		"fixVersions": [
			{ "set": [{"name": "%s"}] }
		]
	}}`, version)

	return UpdateIssue(id, []byte(update)), nil
}
