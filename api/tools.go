package api

import (
	"strconv"
	"strings"
	"time"
)

func NextVersion(current string) string {
	idx := strings.Split(current, "-")
	loc, _ := time.LoadLocation("Europe/Paris")
	currentVersionDate, _ := time.ParseInLocation("20060102", idx[0], loc)
	counter := idx[1]

	now := time.Now()

	var counterInt int

	if now.Year() == currentVersionDate.Year() && now.YearDay() == currentVersionDate.YearDay() {
		counterInt, _ = strconv.Atoi(counter)
		counterInt = counterInt + 1
	} else {
		counterInt = 1
	}

	newVersion := now.Format("20060102") + "-" + strconv.Itoa(counterInt)
	return newVersion
}
