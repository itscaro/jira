package api

type ProjectResponse struct {
	Expand      string `json:"expand"`
	Self        string `json:"self"`
	ID          string `json:"id"`
	Key         string `json:"key"`
	Description string `json:"description"`
	Lead        struct {
		Self       string `json:"self"`
		Key        string `json:"key"`
		Name       string `json:"name"`
		AvatarUrls struct {
			One6X16   string `json:"16x16"`
			Two4X24   string `json:"24x24"`
			Three2X32 string `json:"32x32"`
			Four8X48  string `json:"48x48"`
		} `json:"avatarUrls"`
		DisplayName string `json:"displayName"`
		Active      bool   `json:"active"`
	} `json:"lead"`
	Components []struct {
		Self                string `json:"self"`
		ID                  string `json:"id"`
		Name                string `json:"name"`
		IsAssigneeTypeValid bool   `json:"isAssigneeTypeValid"`
	} `json:"components"`
	IssueTypes []struct {
		Self        string `json:"self"`
		ID          string `json:"id"`
		Description string `json:"description"`
		IconURL     string `json:"iconUrl"`
		Name        string `json:"name"`
		Subtask     bool   `json:"subtask"`
		AvatarID    int    `json:"avatarId,omitempty"`
	} `json:"issueTypes"`
	AssigneeType string    `json:"assigneeType"`
	Versions     []Version `json:"versions"`
	Name         string    `json:"name"`
	Roles        struct {
		AtlassianAddonsProjectAccess string `json:"atlassian-addons-project-access"`
		Developers                   string `json:"Developers"`
		ReadOnlyUsers                string `json:"Read Only Users"`
		ProjectWatchers              string `json:"Project Watchers"`
		ScrumMaster                  string `json:"Scrum Master"`
		Administrators               string `json:"Administrators"`
		Users                        string `json:"Users"`
		TempoProjectManagers         string `json:"Tempo Project Managers"`
	} `json:"roles"`
	AvatarUrls struct {
		Four8X48  string `json:"48x48"`
		Two4X24   string `json:"24x24"`
		One6X16   string `json:"16x16"`
		Three2X32 string `json:"32x32"`
	} `json:"avatarUrls"`
	ProjectKeys     []string `json:"projectKeys"`
	ProjectCategory struct {
		Self        string `json:"self"`
		ID          string `json:"id"`
		Name        string `json:"name"`
		Description string `json:"description"`
	} `json:"projectCategory"`
	ProjectTypeKey string `json:"projectTypeKey"`
}

type Version struct {
	Self            string `json:"self,omitempty"`
	ID              string `json:"id,omitempty"`
	Name            string `json:"name,omitempty"`
	Archived        bool   `json:"archived,omitempty"`
	Released        bool   `json:"released,omitempty"`
	ReleaseDate     string `json:"releaseDate,omitempty"`
	UserReleaseDate string `json:"userReleaseDate,omitempty"`
	Project         string `json:"project,omitempty"`
	ProjectID       int    `json:"projectId,omitempty"`
	Description     string `json:"description,omitempty"`
	StartDate       string `json:"startDate,omitempty"`
	UserStartDate   string `json:"userStartDate,omitempty"`
	Overdue         bool   `json:"overdue,omitempty"`
}

type VersionPaginated struct {
	Self       string    `json:"self"`
	NextPage   string    `json:"nextPage"`
	MaxResults int       `json:"maxResults"`
	StartAt    int       `json:"startAt"`
	Total      int       `json:"total"`
	IsLast     bool      `json:"isLast"`
	Values     []Version `json:"values"`
}
