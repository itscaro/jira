PACKAGE_NAME=bitbucket.org/itscaro/$(basename $PWD)
docker run --rm -it -v "$PWD":/go/src/$PACKAGE_NAME -w /go/src/$PACKAGE_NAME golang:1.7 sh /go/src/$PACKAGE_NAME/run.sh
