package main

import (
	"log"
	"os"

	"bitbucket.org/itscaro/jira/cmd"
)

func init() {
}

func main() {
	f, err := os.Create("log.log")
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Printf("Starting... (PID %d)\n", os.Getpid())

	cmd.Execute()
}

func recoverFunc() {
	if r := recover(); r != nil {
		log.Println("Recovered in f", r)
	}
}
