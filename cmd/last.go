// Copyright © 2016 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"errors"
	"fmt"

	"bitbucket.org/itscaro/jira/api"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	ErrCannotGuestNextVersion = errors.New("Cannot guest next version")
)

// lastCmd represents the last command
var lastCmd = &cobra.Command{
	Use:   "last",
	Short: "A brief description of your command",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("last called")

		versions, _, _ := getLastVersions()

		for k, v := range versions {
			fmt.Printf("%s\t%s\n", k, v)
		}
	},
}

func init() {
	versionCmd.AddCommand(lastCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// lastCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// lastCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}

func getLastVersions() (map[string]string, string, error) {
	versions := map[string]string{}
	versionsUnified := map[string]string{}

	for _, v := range viper.GetStringSlice("jira.projects") {
		versions[v] = api.GetLastVersion(v)
		versionsUnified[versions[v]] = versions[v]
	}

	if len(versionsUnified) > 1 {
		return versions, "", ErrCannotGuestNextVersion
	}

	var newVersion string
	for _, v := range versionsUnified {
		newVersion = api.NextVersion(v)
	}

	return versions, newVersion, nil
}
