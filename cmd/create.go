// Copyright © 2016 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"bitbucket.org/itscaro/jira/api"

	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// create-nextCmd represents the create-next command
var createCmd = &cobra.Command{
	Use:   "create [version-to-create]",
	Short: "Create new version for all projects, if no version specified, I will guess the next version to create",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("create called")

		if flagDryRun {
			fmt.Println("=== IN DRY-RUN MODE ===")
		}

		var newVersion string

		if len(args) == 1 {
			newVersion = args[0]
		} else {
			versions, newVersion, err := getLastVersions()
			if err == ErrCannotGuestNextVersion {
				fmt.Println(err.Error())
				fmt.Println("Last versions detected:")

				for k, v := range versions {
					fmt.Printf("%s\t%s\n", k, v)
				}

				os.Exit(1)
			}
			fmt.Println("Version to be created: ", newVersion)
		}

		for _, p := range viper.GetStringSlice("jira.projects") {
			if !flagDryRun {
				api.CreateNewVersion(p, newVersion)
			}

			fmt.Printf("%s\t%s\n", p, newVersion)
		}
	},
}

func init() {
	versionCmd.AddCommand(createCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// create-nextCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// create-nextCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
