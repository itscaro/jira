// Copyright © 2016 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"bitbucket.org/itscaro/jira/api"
	"github.com/spf13/cobra"
)

// linkissueCmd represents the linkissue command
var linkissueCmd = &cobra.Command{
	Use:   "linkissue",
	Short: "A brief description of your command",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("linkissue called")

		if flagDryRun {
			fmt.Println("=== IN DRY-RUN MODE ===")
		}

		for _, issue := range flagIssues {
			if !flagDryRun {
				api.UpdateIssueVersion(issue, flagVersion)
			}

			fmt.Printf("Link %s to %s\n", issue, flagVersion)
		}
	},
}

var (
	flagIssues  []string
	flagVersion string
)

func init() {
	versionCmd.AddCommand(linkissueCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// linkissueCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	linkissueCmd.Flags().StringSliceVarP(&flagIssues, "issue", "i", []string{}, "Issue to link")
	linkissueCmd.Flags().StringVarP(&flagVersion, "version", "v", "", "Version to link")

}
