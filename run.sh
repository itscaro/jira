#!/bin/bash

go get -v

OS="darwin linux windows"

#ARCH="386 amd64"
ARCH="amd64"

NAME=jira

go-bindata -o cmd/bindata.go -pkg cmd assets templates/html

for GOOS in $OS; do
   for GOARCH in $ARCH; do
     go build -v -o build/$NAME-$GOOS-$GOARCH
   done
done
